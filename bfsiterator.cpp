#include "bfsiterator.h"


BFSiterator::BFSiterator(Graph *graph, Iterator *parent):
                         Iterator("BFS",graph, parent){
    this->iterate(graph);
}

BFSiterator::BFSiterator(Iterator *parent):Iterator{"Iteracja BFS", parent}{
    connect(this,SIGNAL(triggered(bool)),Graph::getInstance(), SLOT(startIterateBFS()));
}

void BFSiterator::iterate(Graph *graph){
    int node = 0;
    queue.push(node);
    std::vector<std::list<int>> graphList = graph->getGraphList();
    for(int i = 0; i<graphList.size(); i++){
        graphList.at(i).sort();
    }
    std::vector<int> visitedNodes;
    std::vector<int> inQueue;
    inQueue.push_back(node);
    while(!queue.empty()){
        int front = queue.front();
        queue.pop();
        inQueue.pop_back();
        visitedNodes.push_back(front);
        while(!graphList.at(front).empty()){
            int wasVisited {false};
            for(int i = 0; i<visitedNodes.size(); i++){
                if(visitedNodes.at(i) == graphList.at(front).front()){
                    wasVisited = true;
                }
            }
            if(!wasVisited && !isInQueue(queue, graphList.at(front).front())){
                queue.push(graphList.at(front).front());
                inQueue.push_back(queue.front());
            }
            graphList.at(front).pop_front();
            }
        }
        std::vector<QPointF> nodePosition = graph->getGraphNodePosition();
        for(int i = 0; i<visitedNodes.size(); i++){
            nodeVisited.push_back(nodePosition.at(visitedNodes.at(i)));
       }


 }


std::vector<QPointF> BFSiterator::getVisitedNodes(){
    return nodeVisited;
}

bool BFSiterator::isInQueue(std::queue<int> que, int value){
    while(!que.empty()){
        if(que.front() == value)
            return true;
        que.pop();
    }
    return false;
}
