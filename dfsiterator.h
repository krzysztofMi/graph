#ifndef DFS_HI
#define DFS_HI

#include <stack>
#include "iterator.h"

class DFSiterator: public Iterator{
    std::stack<int> stack;
protected:
    virtual void iterate(Graph*);
public:
    explicit DFSiterator(Graph*, Iterator *parent = nullptr);
    explicit DFSiterator(Iterator *parent = nullptr);
    virtual std::vector<QPointF> getVisitedNodes();
};

#endif // DFS_HI
