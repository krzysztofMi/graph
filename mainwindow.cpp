#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Graph");



    //PLIK MENU
    menuBar = new QMenuBar();
    plik = new QMenu("Plik");
    menuBar ->addMenu(plik);
    reader = new Reader();

    writter = new Writter();
    plik ->addMenu(reader);
    plik ->addMenu(writter);
    this->setMenuBar(menuBar);
    //ITERATION MENUw
    iterMenu = new QMenu("Iteracja");
    menuBar->addMenu(iterMenu);
    iter = new Iterator();
    bfsIter = new BFSiterator();
    dfsIter = new DFSiterator();
    iterMenu->addAction(iter);
    iterMenu->addAction(dfsIter);
    iterMenu->addAction(bfsIter);
    graph = Graph::getInstance();
    this->setCentralWidget(graph);
    label = new QLabel(this);
    QRect labelGeometry {0,690,50,25};
    label->setGeometry(labelGeometry);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mouseMoveEvent(QMouseEvent *event){
    QPoint mousePosition = event->pos();
    QString string{QString::number(mousePosition.rx())+" "+QString::number(mousePosition.ry())};//+" "+mousePosition.ry())};
    label->setText(string);
    graph->getMousePosition(mousePosition);
}

void MainWindow::paintEvent(QPaintEvent *event){
    QPainter painter(this);

    painter.fillRect(0, 0, 3000, 3000, Qt::green);
    painter.fillRect(label->geometry(),Qt::white);
}
