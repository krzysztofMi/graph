#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <QWidget>
#include <QToolBar>
#include <QGraphicsScene>
#include <QTimer>
#include <QGraphicsScene>
#include <QEventLoop>
#include <QDebug>
class Graph : public QWidget
{
    Q_OBJECT
    int counter;
    float circleRay;
    bool iterate;
    bool iterationType[3] {0};
    QPointF lastMousePosition;
    QPointF mouseMoveOffset;
    std::vector<QPointF> vertexPosition;
    static Graph *instance;
    explicit Graph(QWidget *parent = nullptr);
    int vertexNumber;
    std::vector<std::list<int>> graphList;
    QToolBar *graphToolBar;
    QAction *addVertexA;
    QAction *removeVertexA;
    QAction *modifyVertexA;
    QTimer *timer;
    void setGraph(std::vector<std::list<int>>&, std::vector<QPointF>&);
public:
   static Graph* getInstance();
   void showList();
   void getMousePosition(QPoint);
   std::vector<std::list<int>> getGraphList();
   std::vector<QPointF> getGraphNodePosition();
protected:
    void paintEvent(QPaintEvent*);
    void mouseDoubleClickEvent(QMouseEvent*);
    void mousePressEvent(QMouseEvent*);
signals:
public slots:
    void addVertex();
    void removeVertex();
    void modifyVertex();
    void animation();
    void startIterate();
    void startIterateBFS();
    void startIterateDFS();
public:
    friend class Reader;
    friend class Iterator;
};
#endif // GRAPH_H
