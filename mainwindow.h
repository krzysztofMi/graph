#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QLabel>
#include <QPainter>
#include "graph.h"
#include "reader.h"
#include "writter.h"
#include "iterator.h"
#include "bfsiterator.h"
#include "dfsiterator.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
protected:
    void mouseMoveEvent(QMouseEvent*);
    void paintEvent(QPaintEvent *event);
private:
    Ui::MainWindow *ui;
    QMenuBar *menuBar;
    QMenu *plik;
    QMenu *iterMenu;
    Graph *graph;
    QLabel *label;
    Reader *reader;
    Writter *writter;
    Iterator *iter;
    QAction *bfsIter;
    QAction *dfsIter;
};

#endif // MAINWINDOW_H
