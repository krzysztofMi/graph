#ifndef ITERATOR_H
#define ITERATOR_H

#include <QWidget>
#include <QAction>
#include "graph.h"

class Iterator: public QAction{
    Q_OBJECT
protected:
    const QString type;
    std::vector<QPointF> nodeVisited;
    virtual void iterate(Graph*);
    explicit Iterator(QString, Graph*, QAction *parent = nullptr);
    explicit Iterator(QString, QAction *parent=nullptr);
public:
    explicit Iterator(QAction *parent = nullptr);
    explicit Iterator(Graph*, QAction *parent = nullptr);
    virtual std::vector<QPointF> getVisitedNodes();
};
#endif // ITERATOR_H
