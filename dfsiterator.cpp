#include <set>
#include "dfsiterator.h"
#include <QDebug>

DFSiterator::DFSiterator(Graph *graph, Iterator *parent):
                         Iterator("DFS",graph, parent){
    this->iterate(graph);
}

DFSiterator::DFSiterator(Iterator *parent):Iterator{"Iteracja DFS", parent}{
    connect(this,SIGNAL(triggered(bool)),Graph::getInstance(), SLOT(startIterateDFS()));
}

void DFSiterator::iterate(Graph *graph){
    int node = 0;
    stack.push(node);
    std::vector<int> visitedNodes;
    std::vector<std::list<int>> graphList = graph->getGraphList();
    for(int i = 0; i<graphList.size(); i++){
        graphList.at(i).sort();
    }
    while(!stack.empty()){
        int top = stack.top();
        stack.pop();
        visitedNodes.push_back(top);
        while(!graphList.at(top).empty()){
            bool wasVisited = false;
            for(int i = 0; i<visitedNodes.size(); i++){
                if(visitedNodes.at(i) == graphList.at(top).back()){
                    wasVisited = true;
                }
            }
            if(!wasVisited){
                stack.push(graphList.at(top).back());
            }
                graphList.at(top).pop_back();
        }
    }
    std::vector<QPointF> nodePosition = graph->getGraphNodePosition();
    for(int i = 0; i<visitedNodes.size(); i++){
        nodeVisited.push_back(nodePosition.at(visitedNodes.at(i)));
    }
}

std::vector<QPointF> DFSiterator::getVisitedNodes(){
    return nodeVisited;
}
