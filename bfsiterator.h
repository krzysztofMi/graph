#ifndef BFSITERATOR_H
#define BFSITERATOR_H
#include "iterator.h"
#include<QAction>
#include<queue>
#include "graph.h"

class BFSiterator: public Iterator{
    std::queue<int> queue;
protected:
    virtual void iterate(Graph*);
public:
    explicit BFSiterator(Graph*, Iterator *parent = nullptr);
    explicit BFSiterator(Iterator *parent = nullptr);
    virtual std::vector<QPointF> getVisitedNodes();
    bool isInQueue(std::queue<int>, int);
};

#endif // BFSITERATOR_H
