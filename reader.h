#ifndef READER_H
#define READER_H

#include <QMenu>
#include <QFile>
#include <QDialog>
#include <QXmlStreamReader>
#include "graph.h"
class Reader:public QMenu{
    Q_OBJECT
    QAction *loadAction;

public:
   explicit Reader(QMenu *parent = nullptr);
public slots:
    void loadFromFile();
};

#endif // READER_H
