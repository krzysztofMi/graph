#include <QFileDialog>
#include <QDomDocument>
#include <QTextStream>
#include <QDebug>
#include "writter.h"


Writter::Writter(QMenu *parent): QMenu("Zapisz do", parent){
    saveAction = new QAction("XML", this);
    this->addAction(saveAction);

    connect(saveAction, SIGNAL(triggered(bool)), this, SLOT(saveToFile()));
}

void Writter::saveToFile(){
    QString fileName = QFileDialog::getSaveFileName(this, tr("Zapisz"),"",tr("XML files (*xml)"));
    if(fileName == ""){
        return;
    }
    QFile file(fileName);
    if(!(file.open(QIODevice::WriteOnly))){
        return;
    }
    QDomDocument document;
    QDomElement root = document.createElement("Graph");
    //document.appendChild(root);

    Graph *graph = Graph::getInstance();
    std::vector<std::list<int>> vector = graph->getGraphList();
    std::vector<QPointF> posVector = graph->getGraphNodePosition();

    for(int i = 0;i<static_cast<int>(vector.size()); i++){
        QDomElement node = document.createElement("node");
        QString positionString{QString::number(posVector.at(i).rx())
                               +","+QString::number(posVector.at(i).ry())};
        node.setAttribute("nodeID", i+1);
        node.setAttribute("position", positionString);
        root.appendChild(node);
        for(auto &element: vector.at(i)){
            QDomElement edge = document.createElement("edge");
            QDomText edgeText = document.createTextNode(QString::number(element+1));
            edge.appendChild(edgeText);
            node.appendChild(edge);
        }
    }
    document.appendChild(root);
    QTextStream stream(&file);
    stream<<document;
    file.close();
}
