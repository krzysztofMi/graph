#ifndef WRITTER_H
#define WRITTER_H

#include <QMenu>
#include <QFile>
#include <QXmlStreamWriter>
#include "graph.h"
class Writter : public QMenu{
    Q_OBJECT
    QAction *saveAction;
public:
    Writter(QMenu *parent = nullptr);
public slots:
    void saveToFile();
};

#endif // WRITTER_H
