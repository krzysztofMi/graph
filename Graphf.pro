#-------------------------------------------------
#
# Project created by QtCreator 2018-12-04T13:22:16
#
#-------------------------------------------------

QT       += core gui
QT       += xml
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Graphf
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    graph.cpp \
    reader.cpp \
    writter.cpp \
    iterator.cpp \
    dfsiterator.cpp \
    bfsiterator.cpp

HEADERS += \
        mainwindow.h \
    graph.h \
    reader.h \
    writter.h \
    iterator.h \
    bfsiterator.h \
    dfsiterator.h

FORMS += \
        mainwindow.ui
