#include "reader.h"
#include <QXmlStreamReader>
#include <QFileDialog>
#include <QDebug>
#include <QStringList>
Reader::Reader(QMenu *parent): QMenu{"Wczytaj z", parent}{
    loadAction = new QAction("XML", this);
    this->addAction(loadAction);
    connect(loadAction, SIGNAL(triggered(bool)),this, SLOT(loadFromFile()));
}

void Reader::loadFromFile(){
    QString fileName = QFileDialog::getOpenFileName(this, tr("Wczytaj"),"",tr("XML files (*xml)"));
    if(fileName == ""){
        return;
    }
    QFile file(fileName);
    if(!(file.open(QIODevice::ReadOnly))){
        return;
    }
    std::vector<std::list<int>> vector;
    std::vector<QPointF> posVector;
    Graph *graph = Graph::getInstance();
    QXmlStreamReader xmlReader(&file);
    int node{-1};
    while(!xmlReader.atEnd()){
        xmlReader.readNext();
        if(xmlReader.name() == "node" && xmlReader.attributes().hasAttribute("position")){
            node++;
            vector.push_back(std::list<int>());
            posVector.push_back(QPointF());
            QString pos = xmlReader.attributes().value("position").toString();
            QStringList posList = pos.split(',');
            QString x =posList.at(0);
            QString y = posList.at(1);
            posVector.at(node).setX(x.toFloat());
            posVector.at(node).setY(y.toFloat());
        }
        if(xmlReader.name() == "edge"){
            vector.at(node).push_back(xmlReader.readElementText().toInt() - 1);
        }
    }
    graph->setGraph(vector, posVector);
    file.close();
}
