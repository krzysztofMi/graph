#include <QPainter>
#include <QMessageBox>
#include <QPushButton>
#include <QInputDialog>
#include <cmath>
#include <unistd.h>
#include <QDebug>
#include <QRectF>
#include <algorithm>
#include <QMouseEvent>
#include <QCursor>
#include <QEventLoop>
#include "graph.h"
#include "iterator.h"
#include "bfsiterator.h"
#include "dfsiterator.h"
Graph::Graph(QWidget *parent) : QWidget(parent){
   vertexNumber = 0;
   circleRay = 25;
   setMouseTracking(true);
   //TOOLBAR
   graphToolBar = new QToolBar(this);
   graphToolBar ->addAction(addVertexA = new QAction("Dodaj wierzchołek"));
   graphToolBar ->addAction(removeVertexA = new QAction("Usuń wierzchołek"));
   graphToolBar ->addAction(modifyVertexA = new QAction("Zmodyfikuj"));

   //ANIMATION
   timer = new QTimer();
   counter = 0;

   //SLOTS&SIGNALS
   connect(timer,SIGNAL(timeout()),this,SLOT(animation()));
   connect(addVertexA, SIGNAL(triggered(bool)), this, SLOT(addVertex()));
   connect(removeVertexA, SIGNAL(triggered(bool)), this, SLOT(removeVertex()));
   connect(modifyVertexA, SIGNAL(triggered(bool)), this, SLOT(modifyVertex()));
}

Graph* Graph::instance = nullptr;
Graph* Graph::getInstance(){
    if(instance == nullptr){
        instance = new Graph();
    }
    return instance;
}

void Graph::paintEvent(QPaintEvent *event){
    QPainter painter(this);
    painter.fillRect(graphToolBar->geometry(), QBrush{QColor(110, 219, 241)});
    float moveX{300};
    float moveY{350};
    float numberOfNodeTextSize{20};
    QPointF activePoint{moveX, moveY};
    for(int i = 0; i<vertexNumber; i++){
        for(int i = 0; i<vertexNumber; i++){
            for(auto &elem: graphList.at(i)){
                painter.drawLine(vertexPosition.at(i), vertexPosition.at(elem));
                painter.setBrush(QBrush{Qt::white});
                painter.drawEllipse(vertexPosition.at(elem), circleRay, circleRay);
            }
             painter.setBrush(QBrush{Qt::white});
             painter.drawEllipse(vertexPosition.at(i), circleRay, circleRay);
            //ITERATOR animation
            if(iterate == true){
                std::vector<QPointF> visitedNodes;
                Iterator *it;
                if(iterationType[0]){
                    it = new Iterator(this);
                    visitedNodes = it->getVisitedNodes();
                }
                else if(iterationType[1]){
                    it = new BFSiterator(this);
                    visitedNodes = it->getVisitedNodes();
                }
                else if(iterationType[2]){
                    it = new DFSiterator(this);
                    visitedNodes = it->getVisitedNodes();
                }
                for(int j = 0; j<counter; j++){
                    painter.setBrush(QBrush{Qt::gray});
                    painter.drawEllipse(visitedNodes.at(j), circleRay, circleRay);
                }
        }
        for(int i = 0; i<vertexNumber; i++){
            QRectF rect1(vertexPosition.at(i).rx() - 3, vertexPosition.at(i).ry() -7,
            numberOfNodeTextSize, numberOfNodeTextSize);
            painter.setPen(QPen{Qt::red});
            painter.drawText(rect1, QString::number(i+1));
        }
    }
    }
}
void Graph::mouseDoubleClickEvent(QMouseEvent *event){
    graphList.push_back(std::list<int>());
    vertexNumber++;
    vertexPosition.push_back(QPointF{static_cast<float>(event->pos().rx()),
                                     static_cast<float>(event->pos().ry())});
    update();
}

void Graph::mousePressEvent(QMouseEvent *event){
    mouseMoveOffset = event->pos();
}

void Graph::getMousePosition(QPoint position){
    int vertex{0};
    bool isInVertex{false};
    for(int i = 0; i<vertexNumber; i++){
        float pom{static_cast<float>(pow(mouseMoveOffset.rx()-vertexPosition.at(i).rx(),2))+
                  static_cast<float>(pow(mouseMoveOffset.ry()-vertexPosition.at(i).ry(),2))};
        if(pom<pow(circleRay, 2)){
            isInVertex = true;
            vertex = i;
        }
    }
    if(isInVertex){
        vertexPosition.at(vertex) = position;
        mouseMoveOffset = position;
        repaint();
    }
}

void Graph::addVertex(){
    bool ok{false};
    float newVertexX = QInputDialog::getDouble(this, tr("Podaj współrzędną X punktu: "),
                      tr("Podaj punkt X"), 0, 0, 1000, 1, &ok);
    if(ok){
        float newVertexY = QInputDialog::getDouble(this, tr("Podaj współrzędną Y punktu: "),
                          tr("Podaj punkt Y"), 0, 0, 1000, 1, &ok);
        if(ok){
            vertexNumber++;
            graphList.push_back(std::list<int>());
            vertexPosition.push_back(QPointF{newVertexX, newVertexY});
        }
    }
}
void Graph::showList(){
        int i = 1;
        for(auto &elem:graphList){
            std::cout<<i<<": ";
            i++;
            for(auto &el:elem){
                std::cout<<el<<" ";
            }
            std::cout<<std::endl;
        }
}
/*
void Graph::removeVertex(){
    if(vertexNumber>0){
        bool ok;
        int node = QInputDialog::getInt(this, "Usuń wierzhchołek","ok",
                                        0,1,vertexNumber,1,&ok);
        if(ok){
            for(int i = 0; i <vertexNumber;i++){
                    graphList.at(i).remove(node-1);
            }
            graphList.at(node-1).clear();
            graphList.erase(graphList.begin()+node-1);
            vertexPosition.erase(vertexPosition.begin()+node-1);
            vertexNumber--;
        }
    }
}*/

void Graph::removeVertex(){
    if(vertexNumber>0){
        for(int i = 0; i <vertexNumber;i++){
                graphList.at(i).remove(vertexNumber-1);
        }
        vertexNumber--;
        graphList.pop_back();
        vertexPosition.pop_back();
        update();
    }
}

void Graph::modifyVertex(){
    QMessageBox *msg = new QMessageBox;
    msg->setText("Dodawanie wierzchołków.");
    msg->setInformativeText("Co chcesz zrobić?");
    //QAbstractButton *butt= new QPushButton();
    QPushButton *addButton = msg->addButton(tr("Dodaj"), QMessageBox::AcceptRole);
    QPushButton *removeButton = msg->addButton(tr("Usuń"), QMessageBox::NoRole);
    msg->addButton(tr("Anuluj"), QMessageBox::YesRole);
    msg->exec();
    if(msg->clickedButton() == addButton){
        bool ok1{false};
        bool ok2{false};
        int startVertex = QInputDialog::getInt(this, tr("Dodaj krawędź"),
                          tr("Podaj wierzchołek początkowy"), 0, 1, vertexNumber, 1, &ok1);
        int endVertex = QInputDialog::getInt(this, tr("Dodaj krawędź"),
                          tr("Podaj wierzchołek końcowy"), 0, 1, vertexNumber, 1, &ok2);
        if(ok1&&ok2){
            graphList.at(startVertex-1).push_back(endVertex-1);
            graphList.at(endVertex-1).push_back(startVertex-1);
        }
    }
    else if(msg->clickedButton() == removeButton){
        bool ok1{false};
        bool ok2{false};
        int startVertex = QInputDialog::getInt(this, tr("Dodaj krawędź"),
                          tr("Podaj wierzchołek początkowy"), 0, 1, vertexNumber, 1, &ok1);
        int endVertex = QInputDialog::getInt(this, tr("Dodaj krawędź"),
                          tr("Podaj wierzchołek końcowy"), 0, 1, vertexNumber, 1, &ok2);
        if(ok1&&ok2){
            graphList.at(startVertex-1).remove(endVertex-1);
            graphList.at(endVertex-1).remove(startVertex-1);
        }
    }
    else{
        return;
    }
    update();
}

void Graph::setGraph(std::vector<std::list<int>> &vector, std::vector<QPointF> &posVector){
    vertexNumber = vector.size();
    graphList = vector;
    vertexPosition = posVector;
}

std::vector<std::list<int>> Graph::getGraphList(){
    return graphList;
}

std::vector<QPointF> Graph::getGraphNodePosition(){
    return vertexPosition;
}

void Graph::animation(){
    counter++;
    if(counter>vertexNumber){
        sleep(2);
        counter = 0;
        timer->stop();
        iterate = false;
        for(int i=0; i<3; i++){
            iterationType[i] = false;
        }
    }
    update();
}

void Graph::startIterate(){
    iterate = true;
    iterationType[0] = true;
    timer->setInterval(1000);
    timer->start();
}

void Graph::startIterateBFS(){
    iterate = true;
    iterationType[1] = true;
    timer->setInterval(1000);
    timer->start();
}

void Graph::startIterateDFS(){
    iterate = true;
    iterationType[2] = true;
    timer->setInterval(1000);
    timer->start();
}
