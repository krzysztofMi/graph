#include "iterator.h"

Iterator::Iterator(QString name, Graph *graph, QAction *parent)
                  :QAction{parent}, type{name}{

}

Iterator::Iterator(QString name, QAction *parent): QAction{name, parent}{
connect(this,SIGNAL(toggled(bool)),Graph::getInstance(),SLOT(startIterate()));
}

Iterator::Iterator(QAction *parent): QAction("Iteruj",parent){
    connect(this,SIGNAL(triggered(bool)),Graph::getInstance(), SLOT(startIterate()));
}

Iterator::Iterator(Graph *graph, QAction *parent):
                   QAction{parent}, type{"Iterator"}{
    this->iterate(graph);
}

void Iterator::iterate(Graph *graph){
    for(int i = 0; i<graph->vertexNumber; i++){
        nodeVisited.push_back(graph->vertexPosition.at(i));
    }
}

std::vector<QPointF> Iterator::getVisitedNodes(){
    return nodeVisited;
}
